# Eagle Game Engine

This is a 3D game engine written by me. The main focus of the engine is that it tries to stay out of the way as much as possible and lets the user have full control. This is being developed as both a hobby project and as a learning tool for myself, so don't expect the next AAA engine anytime soon.

## Read todo.md to see what needs to be done

## Current Features  
- Scene File Loading

## Future Features Wishlist
- Level Editor  
- Deferred Lighting

[Documentation](docs/index.md)
