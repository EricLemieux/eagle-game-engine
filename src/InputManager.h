#ifndef ENGINE_INPUT_MANAGER_H
#define ENGINE_INPUT_MANAGER_H

#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <iostream>

/*
    The base input manager should not hold all of this stuff
    The variables and functions to find out what buttons are active should be done by the derived class
    For now it will stay here so that the fps camera still works
*/

class InputManager{
public:
	InputManager(){ mMousePosition = glm::i32vec2(0,0); }
	~InputManager(){}

    virtual void OnKeyDown(SDL_KeyboardEvent *key);
    virtual void OnKeyUp(SDL_KeyboardEvent *key);

    bool IsForward(){ return mForward; }
    bool IsBackward(){ return mBackward; }

    bool IsRight(){ return mRight; }
    bool IsLeft(){ return mLeft; }

    bool IsExit(){ return mExit; }

    void SetMousePosition(unsigned int X, unsigned int Y){ mMousePosition.x = X; mMousePosition.y = Y; }
    glm::i32vec2 GetMousePosition(){ return mMousePosition; }
public:
    glm::i32vec2 mMousePosition = glm::i32vec2(0,0);

    bool mForward = false;
    bool mBackward = false;

    bool mRight = false;
    bool mLeft = false;     

    bool mExit = false;
};

class DefaultInputManager : public InputManager{
public:
    virtual void OnKeyDown(SDL_KeyboardEvent *key);
    virtual void OnKeyUp(SDL_KeyboardEvent *key);
};

#endif