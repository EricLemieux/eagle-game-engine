#include "InputManager.h"

//Base class
void InputManager::OnKeyDown(SDL_KeyboardEvent *key)
{
	//Nothing goes in here because the user extend with whatever they actually want
}

void InputManager::OnKeyUp(SDL_KeyboardEvent *key)
{
	//Nothing goes in here because the user extend with whatever they actually want
}

//Default input class 
void DefaultInputManager::OnKeyDown(SDL_KeyboardEvent *key)
{
    //Camera controls
    switch(key->keysym.sym)
    {
        case SDLK_ESCAPE:
            mExit = true;
        case SDLK_w:
            mForward = true;
            break;
        case SDLK_s:
            mBackward = true;
            break;
        case SDLK_d:
            mRight = true;
            break;
        case SDLK_a:
            mLeft = true;
            break;
        default:
            break;
    }
}

void DefaultInputManager::OnKeyUp(SDL_KeyboardEvent *key)
{
    //Camera controls
    switch(key->keysym.sym)
    {
        case SDLK_ESCAPE:
            mExit = false;
        case SDLK_w:
            mForward = false;
            break;
        case SDLK_s:
            mBackward = false;
            break;
        case SDLK_d:
            mRight = false;
            break;
        case SDLK_a:
            mLeft = false;
            break;
        default:
            break;
    }
}