//Base camera system for the engine that the user can use as a parent class for their custom camera

#ifndef ENGINE_CAMERA_H
#define ENGINE_CAMERA_H

#include "GameObject.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <memory>
#include "InputManager.h"

class Camera : public GameObject {
public:
	Camera(){ mCamAngle = glm::vec2(0.0f, 0.0f); }
	~Camera(){}

public:
	//Overridable
	virtual glm::mat4 Update(unsigned int fps){ return glm::lookAt(glm::vec3(0,0,0), glm::vec3(0,0,1), glm::vec3(0,1,0)); }

public:
	//Getters
	float GetMoveSpeed(){ return mMoveSpeed; }
	float GetTurnSpeed(){ return mTurnSpeed; }
	glm::vec3 GetForward(){ return mForward; }
	glm::vec3 GetRight(){ return mRight; }
	glm::vec3 GetUp(){ return mUp; }

	//Setters
	void SetMoveSpeed(float speed){ mMoveSpeed = speed; }
	void SetTurnSpeed(float speed){ mTurnSpeed = speed; }
	void SetInputManager(std::shared_ptr<InputManager> input){ gInputManager = input; }

protected:
	float mMoveSpeed = 4.0f;

	glm::vec2 mCamAngle = glm::vec2(0.0f, 0.0f);

	glm::vec3 mForward = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 mRight = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 mUp = glm::vec3(0.0f, 0.0f, 0.0f);

	float mTurnSpeed = 0.8f;

	std::shared_ptr<InputManager> gInputManager;
};

#endif //ENGINE_CAMERA_H