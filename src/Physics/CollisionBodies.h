#ifndef ENGINE_COLLISION_BODIES_H
#define ENGINE_COLLISION_BODIES_H

#include <glm/glm.hpp>
#include <GL/glew.h>

/*
  TODO these constructors need to be better at constructing, ie set position and dimentions
*/

class CollisionBody {
public:
  CollisionBody(){}
  virtual ~CollisionBody(){}

public:
  enum BodyType {
    BASE,
    SPHERE,
    BOX
  };

public:
  //Getters
  BodyType GetBodyType(){ return mBodyType; }
  glm::vec3 GetPosition(){ return mPosition; }
  bool GetHasGravity(){ return mHasGravity; }
  bool GetHasPhysics(){ return mHasPhysics; }

  //Setters
  void SetPosition(glm::vec3 pos){ mPosition = pos; } //Position should be set based on actual physics, so dont abuse this idiot
  void SetHasGravity(bool grav){ mHasGravity = grav; }
  void SetHasPhysics(bool phys){ mHasPhysics = phys; }

  void Draw();

protected:
  glm::vec3 mPosition = glm::vec3(0,0,0);
  bool mHasGravity = false;
  bool mHasPhysics = false;

  BodyType mBodyType = BASE;

  //Renderable stuff for debug shapes
  int mIndexCount;
  GLuint vbo;
  GLuint ibo;
  GLuint vao;
};

class CollisionSphere : public CollisionBody {
public:
  CollisionSphere(){ mBodyType = SPHERE; }

public:
  float GetRadius(){ return mRadius; }

  void SetRadius(float rad){ mRadius = rad; }

protected:
  float mRadius = 1.0f;
};

class CollisionBox : public CollisionBody {
public:
  CollisionBox(){ mBodyType = BOX; }

public:
  glm::vec3 GetDimentions(){ return mDimentions; }

  void SetDimentions(glm::vec3 dim){ mDimentions = dim; }

protected:
  glm::vec3 mDimentions = glm::vec3(0,0,0);
};

#endif //ENGINE_COLLISION_BODIES_H
