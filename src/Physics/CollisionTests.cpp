#include "PhysicsSystem.h"

//TODOs
//- Rotations in collision testing

bool PhysicsSystem::CollisionTest(std::shared_ptr<CollisionBody> A, std::shared_ptr<CollisionBody> B) {
	switch(A->GetBodyType()) {
		case CollisionBody::BOX:
			switch(B->GetBodyType()) {
				case CollisionBody::BOX:
					return BoxBoxCollisionTest(std::static_pointer_cast<CollisionBox>(A), std::static_pointer_cast<CollisionBox>(B));
					break;
				case CollisionBody::SPHERE:
					return BoxSphereCollisionTest(std::static_pointer_cast<CollisionBox>(A),std::static_pointer_cast<CollisionSphere>(B));
					break;
			}
			break;
		case CollisionBody::SPHERE:
			switch(B->GetBodyType()) {
				case CollisionBody::BOX:
					return BoxSphereCollisionTest(std::static_pointer_cast<CollisionBox>(B),std::static_pointer_cast<CollisionSphere>(A));
					break;
				case CollisionBody::SPHERE:
					return SphereSphereCollisionTest(std::static_pointer_cast<CollisionSphere>(A),std::static_pointer_cast<CollisionSphere>(B));
					break;
			}
			break;
	}
	//Something went wrong and dont have anything to test against
	//TODO this should throw some type of error
	return false;
}

bool PhysicsSystem::BoxBoxCollisionTest(std::shared_ptr<CollisionBox> A, std::shared_ptr<CollisionBox> B) {
	glm::vec3 Apos = A->GetPosition();
	glm::vec3 Adim = A->GetDimentions();
	glm::vec3 Bpos = B->GetPosition();
	glm::vec3 Bdim = B->GetDimentions();

	if(mWorldDimentions == _2D) {
		if(abs(Apos.x - Bpos.x) * 2.0f <= (Adim.x + Bdim.x) &&
			abs(Apos.y - Bpos.y) * 2.0f <= (Adim.y + Bdim.y)) {
			return true;
		}
		return false;
	}else if(mWorldDimentions == _3D) {
		if(abs(Apos.x - Bpos.x) * 2.0f <= (Adim.x + Bdim.x) &&
			abs(Apos.y - Bpos.y) * 2.0f <= (Adim.y + Bdim.y) &&
			abs(Apos.z - Bpos.z) * 2.0f <= (Adim.z + Bdim.z)) {
			return true;
		}
		return false;
	}

	//TODO This should probably error out better
	return false;
}

bool PhysicsSystem::BoxSphereCollisionTest(std::shared_ptr<CollisionBox> A, std::shared_ptr<CollisionSphere> B) {
	glm::vec3 Apos = A->GetPosition();
	glm::vec3 Adim = A->GetDimentions();
	glm::vec3 Bpos = B->GetPosition();
	float Brad = B->GetRadius();

	if(mWorldDimentions == _2D) {
		if(abs(Apos.x - Bpos.x) <= ((Adim.x * 0.5f) + Brad) &&
			abs(Apos.y - Bpos.y) <= ((Adim.y * 0.5f) + Brad)) {
			return true;
		}
		return false;
	}else if(mWorldDimentions == _3D) {
		if(abs(Apos.x - Bpos.x) <= ((Adim.x * 0.5f) + Brad) &&
			abs(Apos.y - Bpos.y) <= ((Adim.y * 0.5f) + Brad) &&
			abs(Apos.z - Bpos.z) <= ((Adim.z * 0.5f) + Brad)) {
			return true;
		}
		return false;
	}

	//TODO This should probably error out better
	return false;
}

bool PhysicsSystem::SphereSphereCollisionTest(std::shared_ptr<CollisionSphere> A, std::shared_ptr<CollisionSphere> B) {
	glm::vec3 Apos = A->GetPosition();
	float Arad = A->GetRadius();
	glm::vec3 Bpos = B->GetPosition();
	float Brad = B->GetRadius();

	glm::vec3 posDiff = abs(Apos - Bpos);
	float dist = (posDiff.x * posDiff.x) + (posDiff.y * posDiff.y) + (posDiff.z * posDiff.z);
	if(dist < (Arad + Brad) * (Arad + Brad)) {
		return true;
	}

	//TODO this should error out
	return false;
}
