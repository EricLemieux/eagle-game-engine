#include "CollisionBodies.h"

void CollisionBody::Draw() {
	glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glDrawArrays(GL_TRIANGLES, 0, mIndexCount);
}