#include "PhysicsSystem.h"

DefaultPhysicsSystem::DefaultPhysicsSystem() {
	SetGravity(0.001f);
	SetWorldDimentions(_3D);
}


void DefaultPhysicsSystem::Update() {
	unsigned int i = 0, max = mCollisionBodies.size();
	for(; i < max; ++i) {
		if(mCollisionBodies[i]->GetHasGravity()) {
			auto pos = mCollisionBodies[i]->GetPosition();
			pos.y -= GetGravity();

			//TODO should do a check here to make sure that it isnt going to collide with anything

			mCollisionBodies[i]->SetPosition(pos);
		}
	}
}
