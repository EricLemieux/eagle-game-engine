/**	Physics system for the game engine to use
*
*/

#ifndef ENGINE_PHYSICS_SYSTEM_H
#define ENGINE_PHYSICS_SYSTEM_H

#include "CollisionBodies.h"
#include <memory>
#include <vector>

class PhysicsSystem {
public:
	PhysicsSystem(){}
	~PhysicsSystem(){}

	enum Dimentions {
		_2D,
		_3D
	};

	//Member functions
	virtual void Update(){}

	bool CollisionTest(std::shared_ptr<CollisionBody> A, std::shared_ptr<CollisionBody> B);

	bool BoxBoxCollisionTest(std::shared_ptr<CollisionBox> A, std::shared_ptr<CollisionBox> B);

	bool BoxSphereCollisionTest(std::shared_ptr<CollisionBox> A, std::shared_ptr<CollisionSphere> B);

	bool SphereSphereCollisionTest(std::shared_ptr<CollisionSphere> A, std::shared_ptr<CollisionSphere> B);

public:
	//Getters
	float GetGravity(){ return mGravity; }
	Dimentions GetWorldDimentions(){ return mWorldDimentions; }

public:
	//Setters
	void SetGravity(float gravity){ mGravity = gravity; }
	void SetWorldDimentions(Dimentions dimentions){ mWorldDimentions = dimentions; }

public:
	std::vector<std::shared_ptr<CollisionBody>> mCollisionBodies;

private:
	float mGravity = 0.0f;
	Dimentions mWorldDimentions = _3D;
};

//Default version of the physics system
class DefaultPhysicsSystem:public PhysicsSystem {
public:
	DefaultPhysicsSystem();
	~DefaultPhysicsSystem(){}

	virtual void Update();
};

#endif //ENGINE_PHYSICS_SYSTEM_H
