//
// Created by eric on 5/18/15.
//

#include "Log.h"

Log::Log(const char* fileName)
{
    mFileLocation = std::string(fileName) + ".log";
}

Log::~Log()
{
    WriteToFile();
}

bool Log::Write(std::string buffer)
{
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    
    std::ostringstream oss;
    oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
    auto str = oss.str();

    mBuffer += str + '\t' + buffer + '\n';

    return true;
}

bool Log::WriteToFile()
{
    std::fstream file(mFileLocation.c_str(), std::fstream::out);

    file<<mBuffer.c_str();

    file.close();

    return true;
}