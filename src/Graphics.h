//
// Created by eric on 6/15/15.
//

#ifndef ENGINE_GRAPHICS_H
#define ENGINE_GRAPHICS_H

#include <memory>
#include <vector>
#include "Model.h"
#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Engine.h"
#include "Shaders.h"

class SceneManager;

class GraphicsSystem
{

public:
    GraphicsSystem();
    ~GraphicsSystem();

public:
    bool Create();
    void PreRender();
    void Render(std::shared_ptr<SceneManager> scene);
    void PostRender();

    //The note allows the user to add a note to the log file in case there is actually an error found; a good use could be where you are calling the error check from.
    void CheckForOpenGLErrors(const char* note);

    //This generates a grid plane, usefull for debugging purposes
    //Stride is the distance between the grid lines
    //Size is the total maximum size of the grid surface
    void GenerateGrid(float stride, float size);

    void LoadSettingsFromConfigFile(const char* filePath);

    /*
        Gets
    */

    float GetFov(){return mFov;}
    float GetNearClippingPlane(){return mNearClippingPlane;}
    float GetFarClippingPlane(){return mFarClippingPlane;}
    float GetWidth(){return mWidth;}
    float GetHeight(){return mHeight;}

    glm::mat4 GetViewMatrix(){return mView;}
    glm::mat4 GetProjectionMatrix(){return mProj;}

    SDL_Window* GetWindow(){return window;}

    /*
        Sets
    */

    void SetViewMatrix(glm::mat4 t){mView = t;}

private:
    SDL_Window* window;
    SDL_GLContext context;

    glm::mat4 mView, mProj;

    bool mDrawGrid=false;
    GLuint mGridVBO, mGridVAO;
    std::vector<float> mGridBuffer;
    Program mGridShaderProgram;

    //Basic info about the graphics
    float mFov, mNearClippingPlane, mFarClippingPlane, mWidth, mHeight;
};

#endif //ENGINE_GRAPHICS_H
