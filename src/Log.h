//
// Created by eric on 5/18/15.
//

#ifndef ENGINE_LOG_H
#define ENGINE_LOG_H

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <ctime>


class Log {
public:
    Log(const char* fileName);
    ~Log();

public:
    //Write to the log buffer
    bool Write(std::string buffer);

    //Write the log to a file on the hard disk
    bool WriteToFile();

private:
    std::string mBuffer;
    std::string mFileLocation;
};


#endif //ENGINE_LOG_H
