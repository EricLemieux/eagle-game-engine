//
// Created by eric on 6/15/15.
//

#include "Graphics.h"

GraphicsSystem::GraphicsSystem()
{
    mFov = 90.0f;
    mWidth = 1280.0f;
    mHeight = 720.0f;
    mNearClippingPlane = 0.01f;
    mFarClippingPlane = 1000.0f;

    ServiceLocator::GetLog()->Write("Graphics system created.");
}

GraphicsSystem::~GraphicsSystem()
{
    ServiceLocator::GetLog()->Write("Starting to shutdown graphics system. Deleting context.");
    SDL_GL_DeleteContext(context);

    ServiceLocator::GetLog()->Write("Destroying Window.");
    SDL_DestroyWindow(window);

    ServiceLocator::GetLog()->Write("Quiting SDL.");
    SDL_Quit();
}

bool GraphicsSystem::Create()
{
    ServiceLocator::GetLog()->Write("Starting to create the graphics system.");

    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        ServiceLocator::GetLog()->Write("Error initing sdl components");
        return false;
    }

    this->window = SDL_CreateWindow("Hello World", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, mWidth, mHeight, SDL_WINDOW_OPENGL);
    this->context = SDL_GL_CreateContext(window);

    ServiceLocator::GetLog()->Write("Window and Context Created.");

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if(err != GLEW_OK)
    {
        ServiceLocator::GetLog()->Write("Error with GLEW: "+std::string((char*)glewGetErrorString(err)));
        return false;
    }

    SDL_GL_MakeCurrent(window, context);
    SDL_GL_SetSwapInterval(0);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    //Log some information from openGL
    ServiceLocator::GetLog()->Write("OpenGL Information");
    ServiceLocator::GetLog()->Write("\tGPU Vendor: " + std::string((char*)glGetString(GL_VENDOR)));
    ServiceLocator::GetLog()->Write("\tRenderer: " + std::string((char*)glGetString(GL_RENDERER)));
    ServiceLocator::GetLog()->Write("\tOpenGL Version: " + std::string((char*)glGetString(GL_VERSION)));
    ServiceLocator::GetLog()->Write("\tOpenGL Shading Language Version: " + std::string((char*)glGetString(GL_SHADING_LANGUAGE_VERSION)));

    //TODO: This shit should be somewhere else
    //Should load this from a file or something, or at least extend it in some way but for now fuck it
    mView = glm::lookAt(glm::vec3(0,1,0), glm::vec3(0,0,1), glm::vec3(0,1,0));
    mProj = glm::perspective(glm::radians(mFov), mWidth/mHeight, mNearClippingPlane, mFarClippingPlane);

    ServiceLocator::GetLog()->WriteToFile();

    return true;
}

void GraphicsSystem::Render(std::shared_ptr<SceneManager> scene)
{

    //Draw all of the models here
    unsigned int count = scene->mObjects.size(), i=0;
    for(;i<count;++i)
    {
        if(scene->mObjects[i]->GetModel() != NULL){
            scene->mObjects[i]->GetModel()->GetShaderProgram().Use();

            //Send the view and projection matrix to the shader
            auto shaderProgram = scene->mObjects[i]->GetModel()->GetShaderProgram();
            glUniformMatrix4fv(shaderProgram.uView, 1, GL_FALSE, glm::value_ptr(mView));
            glUniformMatrix4fv(shaderProgram.uProj, 1, GL_FALSE, glm::value_ptr(mProj));
            glUniformMatrix4fv(shaderProgram.uModel, 1, GL_FALSE, glm::value_ptr(scene->mObjects[i]->GetWorldTransform()));
            glUniform1f(shaderProgram.uHighLight,float(scene->mObjects[i]->GetSelected()));

            scene->mObjects[i]->GetModel()->Draw();
            CheckForOpenGLErrors("Drawing the models");
        }
    }

    //Draw the grid
    if(mDrawGrid)
    {
        mGridShaderProgram.Use();

        //Send the view and projection matrix to the shader
        glUniformMatrix4fv(mGridShaderProgram.uView, 1, GL_FALSE, glm::value_ptr(mView));
        glUniformMatrix4fv(mGridShaderProgram.uProj, 1, GL_FALSE, glm::value_ptr(mProj));
        glUniformMatrix4fv(mGridShaderProgram.uModel, 1, GL_FALSE, glm::value_ptr(glm::mat4()));

        glBindVertexArray(mGridVAO);
        glBindBuffer(GL_ARRAY_BUFFER, mGridVBO);
        glDrawArrays(GL_LINES, 0, mGridBuffer.size()/3);

        CheckForOpenGLErrors("Drawing the grid");
    }
}

void GraphicsSystem::PreRender()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GraphicsSystem::PostRender()
{
    SDL_GL_SwapWindow(window);
}

void GraphicsSystem::CheckForOpenGLErrors(const char* note)
{
    GLenum err = glGetError();
    if(err != GL_NO_ERROR)
    {
        ServiceLocator::GetLog()->Write("OpenGL Error:\t" + std::string(note) + "\t");
        switch (err)
        {
            //All of the descriptions taken from docs.gl/gl3/glGetError
            case GL_INVALID_ENUM:
                ServiceLocator::GetLog()->Write("GL_INVALID_ENUM\tAn unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.");
                break;
            case GL_INVALID_VALUE:
                ServiceLocator::GetLog()->Write("GL_INVALID_VALUE\tA numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.");
                break;
            case GL_INVALID_OPERATION:
                ServiceLocator::GetLog()->Write("GL_INVALID_OPERATION\tThe specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.");
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                ServiceLocator::GetLog()->Write("GL_INVALID_FRAMEBUFFER_OPERATION\tThe framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.");
                break;
            case GL_OUT_OF_MEMORY:
                ServiceLocator::GetLog()->Write("GL_OUT_OF_MEMORY\tThere is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.");
                break;
        };
    }
}

void GraphicsSystem::GenerateGrid(float stride, float size)
{
    glGenVertexArrays(1, &mGridVAO);
    glBindVertexArray(mGridVAO);

    glGenBuffers(1, &mGridVBO);
    glBindBuffer(GL_ARRAY_BUFFER, mGridVBO);

    Shader v = Shader(GL_VERTEX_SHADER), f = Shader(GL_FRAGMENT_SHADER);
    v.LoadFromFile("../assets/shaders/grid_v.glsl");
    f.LoadFromFile("../assets/shaders/grid_f.glsl");
    mGridShaderProgram = Program();
    mGridShaderProgram.Create();
    v.Compile();
    mGridShaderProgram.AttachShader(v);
    f.Compile();
    mGridShaderProgram.AttachShader(f);

    glBindFragDataLocation(mGridShaderProgram.GetId(), 0, "outColour");

    mGridShaderProgram.Link();
    mGridShaderProgram.Use();
    mGridShaderProgram.SetUpUniforms();

    float j = -size;
    for(;j <= size; j += stride)
    {
        mGridBuffer.push_back(-size);
        mGridBuffer.push_back(0.0f);
        mGridBuffer.push_back(j);

        mGridBuffer.push_back(size);
        mGridBuffer.push_back(0.0f);
        mGridBuffer.push_back(j);
    }
    j = -size;
    for(;j <= size; j += stride)
    {
        mGridBuffer.push_back(j);
        mGridBuffer.push_back(0.0f);
        mGridBuffer.push_back(-size);

        mGridBuffer.push_back(j);
        mGridBuffer.push_back(0.0f);
        mGridBuffer.push_back(size);
    }

    glBindVertexArray(mGridVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mGridVBO);
    glBufferData(GL_ARRAY_BUFFER, mGridBuffer.size() * sizeof(float), &mGridBuffer[0], GL_STATIC_DRAW);

    GLint posAttrib = glGetAttribLocation(mGridShaderProgram.GetId(), "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE,  3 * sizeof(GLfloat), (void*)(0 * sizeof(GLfloat)));

    mDrawGrid = true;
}

void GraphicsSystem::LoadSettingsFromConfigFile(const char* filePath)
{
    std::fstream f;
    f.open(filePath, std::fstream::in);
    std::string json;

    if(f.is_open())
    {
        char buffer[2048];

        while (!f.eof())
        {
            f.getline(buffer, 2048);
            json += buffer;
        }
    }

    rapidjson::Document doc;
    doc.Parse(json.c_str());

    const rapidjson::Value& window = doc["window"];

    if(window.HasMember("width"))
        mWidth = window["width"].GetInt();
    if(window.HasMember("height"))
        mHeight = window["height"].GetInt();

    const rapidjson::Value& camera = doc["camera"];

    if(camera.HasMember("fov"))
        mFov = camera["fov"].GetDouble();
    if(camera.HasMember("near"))
        mNearClippingPlane = camera["near"].GetDouble();
    if(camera.HasMember("far"))
        mFarClippingPlane = camera["far"].GetDouble();
}