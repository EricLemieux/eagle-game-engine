//
// Created by eric on 7/5/15.
//

#ifndef ENGINE_GAMEOBJECT_H
#define ENGINE_GAMEOBJECT_H

#include "Log.h"
#include "Model.h"
#include "Physics/CollisionBodies.h"
#include <memory>
#include <string>
#include <glm/glm.hpp>

class GameObject : public std::enable_shared_from_this<GameObject> {
public:
    GameObject();
    ~GameObject(){ServiceLocator::GetLog()->Write("Destroying game object");}

    std::shared_ptr<Model> GetModel(){ return mModel; }

    glm::mat4 GetLocalTransform(){ return mLocalTransform; }
    glm::mat4 GetWorldTransform(){ return mWorldTransform; }

    glm::vec3 GetLocalPosition(){ return glm::vec3(mLocalTransform[3].x, mLocalTransform[3].y, mLocalTransform[3].z); }
    glm::vec3 GetWorldPosition(){ return glm::vec3(mWorldTransform[3].x, mWorldTransform[3].y, mWorldTransform[3].z); }

    void SetTransformation(glm::mat4 t){mLocalTransform = t;}
    void SetPosition(glm::vec3 pos){mLocalTransform[3] = glm::vec4(pos.x, pos.y, pos.z, 1.0f);}

    void SetSelected(bool selected){mSelected = selected;}
    bool GetSelected(){return mSelected;}

    //Scene graph stuff
    void AttachChild(std::shared_ptr<GameObject> child);

    void SetParent(std::shared_ptr<GameObject> parent){ mParent = parent; }
    std::shared_ptr<GameObject> GetParent(){ return mParent; }

    void UpdateChildrenWorldTransform();

    void CreateModel(){ mModel = std::make_shared<Model>(); }

    void GenerateCollisionBodyFromModel();

    //Getters
    std::string GetName(){ return mName; }
    std::shared_ptr<CollisionBox> GetCollisionBody(){ return mCollisionBody; }

    //Setters
    void SetName(std::string name){ mName = name;}

private:
    glm::mat4 mLocalTransform;
    glm::mat4 mWorldTransform;
    std::shared_ptr<Model> mModel = NULL;

    bool mSelected=false;

    std::shared_ptr<GameObject> mParent;
    std::vector<std::shared_ptr<GameObject>> mChildren;

    std::string mName;

    //TODO
    //Need to make this be the base collision body class and have it convert to whatever class is actually needed based on the data. Will probably fix whenever I get internet
    std::shared_ptr<CollisionBox> mCollisionBody;

    Log* gLog;
};


#endif //ENGINE_GAMEOBJECT_H
