//
// Created by eric on 6/15/15.
//

#include "Engine.h"

#include "ServiceLocator.h"

Engine::Engine()
{

    Log* foo = new Log("engine");
    ServiceLocator::SetLog(foo);

    //Set up the global log for the whole engine
    mLog = ServiceLocator::GetLog();

    ServiceLocator::GetLog()->Write("This is from the engine.");
    mLog->Write("Starting Engine.");

    mGraphicsSystem = std::make_shared<GraphicsSystem>();
    mGraphicsSystem->CheckForOpenGLErrors("Check for errors after starting up the graphics system.");

    mPhysicsSystem = std::make_shared<PhysicsSystem>();
    mSceneManager = std::make_shared<SceneManager>(mPhysicsSystem);

    mLog->Write("make cam");
    mCamera = std::make_shared<Camera>();

    mLog->Write("set up input");
    mInputManager = std::make_shared<DefaultInputManager>();

    mGameRunning = true;

    mPrevTime = mCurrentTime  = SDL_GetTicks();
}

Engine::~Engine()
{
    //Kill a bunch of the physics stuff
    //TODO

    mLog->WriteToFile();

    ServiceLocator::GetLog()->WriteToFile();
}


void Engine::Update()
{
    //Calculate the current frame rate, delay to every 100 updates to save cout time.
    static unsigned int num = 0, numLimit = 100, ticsPerSecond = 1000;
    if(num > numLimit)
    {
        num  = 0;
        mCurrentTime = SDL_GetTicks();
        mFPS = (ticsPerSecond*numLimit)/(mCurrentTime - mPrevTime);
        mPrevTime = mCurrentTime;
    }
    num++;

    GetUserInput();

    mGameRunning = !mInputManager->IsExit();

    UpdatePhysics();

    UpdateSceneGraph();

    UpdateCamera();
}

void Engine::UpdateCamera()
{
    auto newView = mCamera->Update(mFPS);

    mGraphicsSystem->SetViewMatrix(newView);

    //Center the mouse after each update so we have a consitent starting position to compare against next update;
    SDL_WarpMouseInWindow(this->mGraphicsSystem->GetWindow(), this->mGraphicsSystem->GetWidth()/2.0f, this->mGraphicsSystem->GetHeight()/2.0f);
}

void Engine::GetUserInput()
{
    while(SDL_PollEvent(&event))
    {
        switch(event.type)
        {
            case SDL_QUIT:
                //Quit the game
                mGameRunning = false;
                break;
            case SDL_KEYDOWN:
                mInputManager->OnKeyDown(&event.key);
                break;
            case SDL_KEYUP:
                mInputManager->OnKeyUp(&event.key);
                break;
            case SDL_MOUSEMOTION: {
                SDL_GetMouseState(&mMousePosition.x, &mMousePosition.y);
                mInputManager->SetMousePosition(mMousePosition.x, mMousePosition.y);
            }
                break;
            case SDL_MOUSEBUTTONDOWN:
                break;
            case SDL_MOUSEBUTTONUP:
                break;
            default:
                //mLog->Write("SDL event type not handled.\n");
                break;
        }
    }
}

void Engine::UpdateSceneGraph()
{
    mSceneManager->GetRootNode()->UpdateChildrenWorldTransform();
}

void Engine::UpdatePhysics()
{
    mPhysicsSystem->Update();
}

glm::vec3 Engine::RayFromMousePos()
{
    //Cant remember what this was for but seems important enough, so going to leave it for now
    /*auto cam = mGraphicsSystem->GetViewMatrix();
    glm::vec3 camPos = glm::vec3(cam[3][0],cam[3][1],cam[3][2]);

    glm::vec3 view = glm::vec3(cam[2][0], cam[2][1], cam[2][2]);
    glm::normalize(view);

    float x = (2.0f * mMousePosition.x) / mGraphicsSystem->GetWidth() - 1.0f;
    float y = 1.0f - (2.0f * mMousePosition.y) / mGraphicsSystem->GetHeight();

    glm::mat4 iVP = glm::inverse(mGraphicsSystem->GetProjectionMatrix() * mGraphicsSystem->GetViewMatrix());
    glm::vec4 worldPos = iVP * glm::vec4(x,y,1.0f,1.0f);
    return glm::normalize(glm::vec3(worldPos));*/
}
