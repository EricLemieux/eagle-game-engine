//
// Created by eric on 6/16/15.
//

#ifndef ENGINE_SHADERS_H
#define ENGINE_SHADERS_H

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include "Log.h"
#include <vector>
#include "ServiceLocator.h"

class Shader
{
public:
    Shader(){}//This is a dummy type just to shut up a different section
    Shader(GLuint shaderType);
    ~Shader();

public:
    bool LoadFromFile(const char* filePath);
    bool LoadFromSource(const char* source);

    std::string GetSource(){return source;}

    void Compile();

    GLuint GetId()const{return id;}

    GLuint GetType()const{return type;}
private:
    std::string source;
    GLuint id;
    GLuint type;
};

class Program
{
public:
    Program(){}
    ~Program();

public:
    void Create();
    void AttachShader(Shader shader);
    void Link();
    void Use();
    void SetUpUniforms();

    Shader GetVertexShader(){ return mVertexShader;}
    Shader GetFragmentShader(){ return mFragmentShader;}

    GLuint GetId()const{return id;}

    //TODO make private
    GLint uModel, uView, uProj;
    GLint uHighLight;

private:
    GLuint id;
    Shader mVertexShader = Shader();
    Shader mFragmentShader = Shader();

    std::shared_ptr<Log> gLog;
};



#endif //ENGINE_SHADERS_H
