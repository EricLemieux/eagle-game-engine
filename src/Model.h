//
// Created by eric on 6/16/15.
//

#ifndef ENGINE_MODEL_H
#define ENGINE_MODEL_H

#include <GL/glew.h>
#include <vector>
#include <stdio.h>
#include <glm/glm.hpp>
#include <fstream>
#include "Log.h"
#include <memory>
#include <string.h>
#include "Shaders.h"
#include <SOIL/SOIL.h>

class Model {
public:
    Model();
    Model(std::shared_ptr<Log> log);
    ~Model(){}

public:
    void SetVerts(std::vector<float> v){mVerts = v;}
    void SetIndices(std::vector<int> i){mIndices = i;}
    void Generate(Program shaderHandle);

    void Draw();

    GLuint GetVAO(){return vao;}

    bool LoadFromFile(const char* filePath);

    Program GetShaderProgram(){ return mShaderProgram; }

    glm::vec3 GetLimits(){ return mLimits; }

    void SetDiffuseTexture(const char* texturePath);

private:
    GLuint vbo;
    GLuint ibo;
    GLuint vao;

    std::vector<float> mVerts;
    std::vector<float> mTexCoords;
    std::vector<float> mNormals;
    std::vector<float> mBufferData;
    std::vector<int> mIndices;

    Program mShaderProgram;

    bool hasDiffuseTexture=false;

    glm::vec3 mLimits;

private:
    bool LoadOBJFromFile(const char* filePath);
};


#endif //ENGINE_MODEL_H
