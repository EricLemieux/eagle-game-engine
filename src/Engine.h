//
// Created by eric on 6/15/15.
//

#ifndef ENGINE_ENGINE_H
#define ENGINE_ENGINE_H

class GraphicsSystem;

#include "Log.h"
#include "Graphics.h"
#include "SceneManager.h"
#include "Camera.h"
#include "InputManager.h"
#include "Physics/PhysicsSystem.h"
#include <SDL2/SDL.h>
#include <memory>
#include <glm/glm.hpp>

class Engine {
public:
    Engine();
    ~Engine();

    static Engine& Instance() {
        static Engine eng;
        return eng;
    }

public:
    std::shared_ptr<GraphicsSystem> GetGraphicsSystem(){return mGraphicsSystem;}
    Log* GetLog(){return mLog;}
    std::shared_ptr<SceneManager> GetSceneManager(){return mSceneManager;}
    std::shared_ptr<Camera> GetCamera(){return mCamera;}
    std::shared_ptr<InputManager> GetInputManager(){return mInputManager;}
    std::shared_ptr<PhysicsSystem> GetPhysicsSystem(){return mPhysicsSystem;}

    void SetCamera(std::shared_ptr<Camera> camera){ mCamera = camera; mSceneManager->GetRootNode()->AttachChild(mCamera); }
    void SetInputManager(std::shared_ptr<InputManager> input){ mInputManager = input; }
    void SetPhysicsSystem(std::shared_ptr<PhysicsSystem> system){ mPhysicsSystem = system; }

private:
    Log* mLog;
    std::shared_ptr<GraphicsSystem> mGraphicsSystem;
    std::shared_ptr<SceneManager> mSceneManager;
    std::shared_ptr<Camera> mCamera;
    std::shared_ptr<InputManager> mInputManager;
    std::shared_ptr<PhysicsSystem> mPhysicsSystem;

public:
    SDL_Event event;

    bool mGameRunning;

    void Update();

    glm::vec3 RayFromMousePos();

private:
    void UpdateCamera();
    void GetUserInput();
    void UpdatePhysics();
    void UpdateSceneGraph();

    unsigned int mPrevTime, mCurrentTime, mFPS;

    int mMouseX;

    glm::i32vec2 mMousePosition;
};


#endif //ENGINE_ENGINE_H
