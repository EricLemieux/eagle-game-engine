//
// Created by eric on 7/5/15.
//

#include "SceneManager.h"

SceneManager::SceneManager(std::shared_ptr<PhysicsSystem> physics) {
    //Create an empty root node so we can attach things to it
    mSceneRootNode = std::make_shared<GameObject>();

    gPhysicsSystem = physics;
}
SceneManager::~SceneManager()
{
    ServiceLocator::GetLog()->Write("Destroying scene");
    ServiceLocator::GetLog()->WriteToFile();
}

//TODO remove the shader handle and find a better way to include that
bool SceneManager::LoadSceneFromFile(const char* filePath, std::shared_ptr<PhysicsSystem> physics)
{
    gPhysicsSystem = physics;

    std::fstream f;
    f.open(filePath, std::fstream::in);
    std::string json;

    if(f.is_open())
    {
        char buffer[2048];

        while (!f.eof())
        {
            f.getline(buffer, 2048);
            json += buffer;
        }
    }

    rapidjson::Document doc;
    doc.Parse(json.c_str());

    const rapidjson::Value& scene = doc["scene"];

    //Create a root node for the objects in the scene to attach to
    mSceneRootNode = std::make_shared<GameObject>();

    for(int i = 0; i < scene.Size(); ++i)
    {
        const rapidjson::Value& obj = scene[i];
        if(!strcmp(obj["type"].GetString(), "gameObject"))
        {
            ProcessGameObject(obj, mSceneRootNode);
        }
    }

    return true;
}

Program SceneManager::CheckShaderAlreadyLoaded(Shader* v, Shader* f)
{
    //TODO this check should be done with a file hash instead of checking the whole file, or at least compare run times for the two
    const unsigned int size = mShaderPrograms.size();
    for(unsigned int i = 0; i < size; ++i)
    {
        if(!strcmp(v->GetSource().c_str(),mShaderPrograms[i].GetVertexShader().GetSource().c_str()))
            if(!strcmp(f->GetSource().c_str(),mShaderPrograms[i].GetFragmentShader().GetSource().c_str()))
                return mShaderPrograms[i];
    }

    //If no match was found then create a new shader program for the object
    Program p = Program();
    p.Create();

    v->Compile();
    p.AttachShader(*v);

    f->Compile();
    p.AttachShader(*f);

    glBindFragDataLocation(p.GetId(), 0, "outColour");

    p.Link();
    p.Use();

    p.SetUpUniforms();

    mShaderPrograms.push_back(p);

    return p;
}

void SceneManager::ProcessGameObject(const rapidjson::Value& sceneObject, std::shared_ptr<GameObject> parent)
{
    //Create object
    std::shared_ptr<GameObject> newObject = std::make_shared<GameObject>();

    //Get positions
    if(sceneObject.HasMember("pos")){
      const rapidjson::Value& pos = sceneObject["pos"];
      newObject->SetPosition(glm::vec3(pos[0].GetDouble(), pos[1].GetDouble(), pos[2].GetDouble()));
    }else{
      newObject->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
    }

    newObject->GetCollisionBody()->SetPosition(newObject->GetWorldPosition());

    //Only make a renderable model if the scene file is supplying a mesh
    if(sceneObject.HasMember("meshPath")){
        newObject->CreateModel();
        auto model = newObject->GetModel();
        newObject->GetModel()->LoadFromFile(sceneObject["meshPath"].GetString());

        std::string name = sceneObject["meshPath"].GetString();

        //Load Texture
        if(sceneObject.HasMember("diffuseTexture"))
            newObject->GetModel()->SetDiffuseTexture(sceneObject["diffuseTexture"].GetString());


        //Load shaders
        std::string vert = "assets/shaders/pass_v.glsl", frag = "assets/shaders/pass_f.glsl";
        if(sceneObject.HasMember("vertex"))
            vert = std::string(sceneObject["vertex"].GetString());
        if(sceneObject.HasMember("fragment"))
            frag = std::string(sceneObject["fragment"].GetString());

        Shader v = Shader(GL_VERTEX_SHADER), f = Shader(GL_FRAGMENT_SHADER);
        v.LoadFromFile(vert.c_str());
        f.LoadFromFile(frag.c_str());

        Program shaderHandle = CheckShaderAlreadyLoaded(&v,&f);

        //Generate the object so we can render it
        newObject->GetModel()->Generate(shaderHandle);

        //Set the name of the object
        if(sceneObject.HasMember("name")) {
            name = sceneObject["name"].GetString();
        }
        newObject->SetName(name);

        //Set if the collision body has physics applied to it
        if(sceneObject.HasMember("physics") && sceneObject["physics"].GetBool() == true) {
            newObject->GetCollisionBody()->SetHasPhysics(true);
            
            gPhysicsSystem->mCollisionBodies.push_back(newObject->GetCollisionBody());
        }

        //Set if the collision body is effected by gravity
        if(sceneObject.HasMember("gravity") && sceneObject["gravity"].GetBool() == true) {
            newObject->GetCollisionBody()->SetHasGravity(true);
        }
    }

    //Populate the children array for the scene graph
    if(sceneObject.HasMember("children")){
      const rapidjson::Value& children = sceneObject["children"];

        for(int i = 0; i < children.Size(); ++i){
            const rapidjson::Value& child = children[i];

            ProcessGameObject(child, newObject);
        }
    }

    mObjects.push_back(newObject);
    parent->AttachChild(newObject);
}