#ifndef ENGINE_CAMERA_FPS_H
#define ENGINE_CAMERA_FPS_H

#include "Camera.h"

class Camera_FPS : public Camera {
public:
	Camera_FPS(){mCamAngle = glm::vec2(0.0f, 0.0f);}
	~Camera_FPS(){}

	virtual glm::mat4 Update(unsigned int fps);

private:
    float mTurnSpeed = 1.2f;
};

#endif //ENGINE_CAMERA_FPS_H