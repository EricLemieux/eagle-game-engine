//
// Created by eric on 7/5/15.
//

#include "GameObject.h"

#include "ServiceLocator.h"

GameObject::GameObject()
{
    mLocalTransform = glm::mat4();

	mName = "";

    mCollisionBody = std::make_shared<CollisionBox>();
}

void GameObject::UpdateChildrenWorldTransform(){

	//Set the local transform based on the physics body
	//TODO This is just position right now should be the whole transform
	// mLocalTransform[3] = glm::vec4(GetCollisionBody()->GetPosition(), 0);

	//Update this objects world transform
	if(mParent == NULL){
		mWorldTransform = mLocalTransform;
	}else{
		mWorldTransform = mParent->GetWorldTransform() + mLocalTransform;
	}

	//Update all of this objects children
	const unsigned int count = mChildren.size();
	for(unsigned int i = 0; i < count; ++i){
		mChildren[i]->UpdateChildrenWorldTransform();
	}
}


void GameObject::AttachChild(std::shared_ptr<GameObject> child){
	//TODO
	//In the future this should figure out the distance between the object and the child to decide what the new local transform

	mChildren.push_back(child);
	child->SetParent(shared_from_this());
}

void GameObject::GenerateCollisionBodyFromModel() {
	mCollisionBody->SetDimentions(mModel->GetLimits() * glm::vec3(2.0f, 2.0f, 2.0f));
}