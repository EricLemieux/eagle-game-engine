//
// Created by eric on 6/16/15.
//

#include "Model.h"

Model::Model()
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
}

Model::Model(std::shared_ptr<Log> log)
{
    //gLog = log;
    Model();
}

void Model::Generate(Program shaderHandle)
{
    mShaderProgram = shaderHandle;

    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, mBufferData.size() * sizeof(float), &mBufferData[0], GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size() * sizeof(int), &mIndices[0], GL_STATIC_DRAW);

    GLint posAttrib = glGetAttribLocation(mShaderProgram.GetId(), "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE,  8 * sizeof(GLfloat), (void*)(0 * sizeof(GLfloat)));

    GLint texCoordAttrib = glGetAttribLocation(mShaderProgram.GetId(), "texcoord");
    glEnableVertexAttribArray(texCoordAttrib);
    glVertexAttribPointer(texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));

    GLint normAttrib = glGetAttribLocation(mShaderProgram.GetId(), "normal");
    glEnableVertexAttribArray(normAttrib);
    glVertexAttribPointer(normAttrib, 3, GL_FLOAT, GL_TRUE, 8 * sizeof(GLfloat), (void*)(5 * sizeof(GLfloat)));

    //if has a texture
    //do the shit
    //TODO
    //diffuseTexture
}

void Model::Draw()
{
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glDrawArrays(GL_TRIANGLES, 0, mIndices.size());
}


bool Model::LoadFromFile(const char *filePath)
{
    //Read the file path and grab the extension so I know how to read it
    //This assumes that the file isn't behind a hidden folder
    char extension[16] = "";
    sscanf(filePath,"%*[^/]%*[^.]%s", &extension);

    if(!strcmp(extension,".obj"))
    {
        return LoadOBJFromFile(filePath);
    }
    else
    {
        //gLog->Write("Model file is not a supported file type:\n\t"+std::string(extension));
        return false;
    }
}

bool Model::LoadOBJFromFile(const char* filePath)
{
    std::fstream f;
    f.open(filePath, std::fstream::in);

    if(f.is_open())
    {
        char buffer[256];

        unsigned int v_max, vt_max, vn_max;
        bool startedFaces = false;

        while(!f.eof())
        {
            f.getline(buffer,256);

            char fw[16] = "";
            sscanf(buffer, "%s", &fw);

            if(!strcmp(fw,"v"))
            {
                float x,y,z;
                sscanf(buffer,"%*[^ ] %f %f %f", &x, &y, &z);
                this->mVerts.push_back(x);
                this->mVerts.push_back(y);
                this->mVerts.push_back(z);

                //Set the vertex limits to be used for roughly defining the physics collision body if none is defined
                if(fabs(x) > mLimits.x)
                    mLimits.x = fabs(x);
                if(fabs(y) > mLimits.y)
                    mLimits.y = fabs(y);
                if(fabs(z) > mLimits.z)
                    mLimits.z = fabs(z);
            }
            else if(!strcmp(fw, "vt"))
            {
                float u,v;
                sscanf(buffer,"%*[^ ] %f %f", &u, &v);
                this->mTexCoords.push_back(u);
                this->mTexCoords.push_back(v);
            }
            else if(!strcmp(fw, "vn"))
            {
                float x,y,z;
                sscanf(buffer,"%*[^ ] %f %f %f", &x, &y, &z);
                this->mNormals.push_back(x);
                this->mNormals.push_back(y);
                this->mNormals.push_back(z);
            }
            else if(!strcmp(fw,"f"))
            {
                if(!startedFaces)
                {
                    v_max = mVerts.size()/3;
                    vt_max = mTexCoords.size()/2;
                    vn_max = mNormals.size()/3;
                    startedFaces = true;
                }

                int v[3]={0,0,0}, vt[3]={0,0,0}, vn[3]={0,0,0};
                //This is assuming the format is something like "f 1/1/1 2/2/2 3/3/3" which is what modeling programs usually generate.
                if(vt_max < 1)  //Sometimes the object doesn't have texture coordinates data
                    sscanf(buffer,"%*[^ ] %i//%i %i//%i %i//%i", &v[0], &vn[0], &v[1], &vn[1], &v[2], &vn[2]);
                else
                    sscanf(buffer,"%*[^ ] %i/%i/%i %i/%i/%i %i/%i/%i", &v[0], &vt[0], &vn[0], &v[1], &vt[1], &vn[1], &v[2], &vt[2], &vn[2]);

                //Cant tell if I am an idiot or what but I am going to reconstruct the buffer so it will actually work
                for(unsigned int i = 0; i < 3; ++i)
                {
                    mBufferData.push_back(mVerts[((v[i]-1)*3)]);
                    mBufferData.push_back(mVerts[((v[i]-1)*3)+1]);
                    mBufferData.push_back(mVerts[((v[i]-1)*3)+2]);
                    if(vt_max < 1)
                    {
                        mBufferData.push_back(0);
                        mBufferData.push_back(0);
                    }
                    else
                    {
                        mBufferData.push_back(mTexCoords[((vt[i]-1)*2)]);
                        mBufferData.push_back(mTexCoords[((vt[i]-1)*2)+1]);
                    }
                    mBufferData.push_back(mNormals[((vn[i]-1)*3)]);
                    mBufferData.push_back(mNormals[((vn[i]-1)*3)+1]);
                    mBufferData.push_back(mNormals[((vn[i]-1)*3)+2]);
                    mIndices.push_back(mIndices.size());
                }
            }
        }

       // gLog->Write("Model File correctly Loaded from path: "+std::string(filePath)+"\n");

        return true;
    }
    else
    {
       // gLog->Write("File could not be found at location\n\t"+std::string(filePath)+"\n");
        return false;
    }
}

void Model::SetDiffuseTexture(const char* texturePath)
{
    hasDiffuseTexture = true;

    GLuint texture;
    glGenTextures(1, &texture);

    int width, height;
    unsigned char* image;

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    image = SOIL_load_image(texturePath, &width, &height, 0, SOIL_LOAD_RGB);
    ServiceLocator::GetLog()->Write(SOIL_last_result());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);
    glUniform1i(glGetUniformLocation(mShaderProgram.GetId(), "diffuseTexture"), 0);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}