#include "Camera_FPS.h"

glm::mat4 Camera_FPS::Update(unsigned int fps){
	//Everything here is going to need the view matrix so might as well only make one copy of it
    glm::mat4 t = GetWorldTransform();

    if(fps == 0)
    	mCamAngle += glm::vec2(0.0f, 0.0f);
    else{
    	mCamAngle.x += mTurnSpeed * (1.0f/fps) * float(1280.0f/2.0f - gInputManager->GetMousePosition().x);
    	mCamAngle.y += mTurnSpeed * (1.0f/fps) * float(720.0f/2.0f - gInputManager->GetMousePosition().y);
    }

    glm::vec3 forward = glm::vec3(cos(mCamAngle.y) * sin(mCamAngle.x), sin(mCamAngle.y), cos(mCamAngle.y) * cos(mCamAngle.x));

    glm::vec3 right = glm::vec3(sin(mCamAngle.x - 3.14f/2.0f), 0.0f, cos(mCamAngle.x - 3.14f/2.0f));
    glm::vec3 up = glm::cross(right, forward);

    glm::vec3 position = glm::vec3(t[3].x, t[3].y, t[3].z);

    if(gInputManager->IsForward())
    {
        position +=  glm::vec3(forward * 1.0f/float(fps) * mMoveSpeed);
    }
    else if(gInputManager->IsBackward())
    {
        position +=  glm::vec3(forward * 1.0f/float(fps) * -mMoveSpeed);
    }
    if(gInputManager->IsRight())
    {
        position +=  glm::vec3(right * 1.0f/float(fps) * mMoveSpeed);
    }
    else if(gInputManager->IsLeft())
    {
        position +=  glm::vec3(right * 1.0f/float(fps) * -mMoveSpeed);
    }

    glm::mat4 newView = glm::lookAt(position, position+forward, up);

    SetPosition(position);

    return newView;
}