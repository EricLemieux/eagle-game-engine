//Service Locator

#ifndef ENGINE_SERVICE_LOCATOR_H
#define ENGINE_SERVICE_LOCATOR_H

#include "Log.h"

class ServiceLocator {
public:
    ServiceLocator(){}
    ~ServiceLocator(){}

    //Setters
    static void SetLog(Log* log) { mLog = log; }

    //Getters
    static Log* GetLog() { return mLog; }

private:
    static Log* mLog;
};

#endif