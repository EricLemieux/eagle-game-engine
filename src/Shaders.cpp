//
// Created by eric on 6/16/15.
//

#include <GL/glew.h>
#include "Shaders.h"

Shader::Shader(GLuint shaderType)
{
    type = shaderType;
}
Shader::~Shader() { }

bool Shader::LoadFromFile(const char* filePath)
{
    std::ifstream f;
    f.open(filePath);

    if(f.is_open())
    {
        std::string s;
        while(!f.eof())
        {
            s += f.get();
        }

        f.close();
        s[s.size()-1] = '\0';
        LoadFromSource(s.c_str());

        return true;
    }
    else
    {
        ServiceLocator::GetLog()->Write("Shader failed to load from file with the path:\t" + std::string(filePath));
        return false;
    }
}

bool Shader::LoadFromSource(const char *src) {
    this->source = (GLchar*)src;
    return true;
}

void Shader::Compile()
{
    std::string buffer = "Starting to compile ";
    buffer += type == GL_FRAGMENT_SHADER ? "Fragment" : "Vertex";
    buffer += " shader";
    ServiceLocator::GetLog()->Write(buffer);

    id = glCreateShader(type);
    const GLchar* s = source.c_str();

    glShaderSource(id, 1, &s, NULL);
    glCompileShader(id);

    GLint c;
    glGetShaderiv(id, GL_COMPILE_STATUS, &c);
    if(c != GL_TRUE)
    {
        ServiceLocator::GetLog()->Write("Shader failed to compile.\tSource:\n"+std::string(source));
        GLchar str[1024];
        glGetShaderInfoLog(id, 1024, 0, str);
        ServiceLocator::GetLog()->Write(std::string(str));
    }
    else
    {
        ServiceLocator::GetLog()->Write("Shader Compiled correctly.");
    }
}

Program::~Program() { }

void Program::AttachShader(Shader shader)
{
    if(shader.GetType() == GL_VERTEX_SHADER)
        mVertexShader = shader;
    else if(shader.GetType() == GL_FRAGMENT_SHADER)
        mFragmentShader = shader;
    else    //TODO log the error
        ServiceLocator::GetLog()->Write("Unregistered shader type");

    glAttachShader(id, shader.GetId());
}

void Program::Create()
{
    id = glCreateProgram();
}

void Program::Link()
{
    glLinkProgram(id);

    GLint a;
    glGetProgramiv(id, GL_LINK_STATUS, &a);

    if(a != GL_TRUE)
        ServiceLocator::GetLog()->Write("Program failed to link properly");
    else
        ServiceLocator::GetLog()->Write("Program linked properly");
}

void Program::Use()
{
    glUseProgram(id);
}

//TODO this should be done more dynamically
void Program::SetUpUniforms()
{
    uModel = glGetUniformLocation(GetId(), "model");
    uView = glGetUniformLocation(GetId(), "view");
    uProj = glGetUniformLocation(GetId(), "proj");

    uHighLight = glGetUniformLocation(GetId(), "highLight");
}
