//
// Created by eric on 7/5/15.
//

#ifndef ENGINE_SCENEMANAGER_H
#define ENGINE_SCENEMANAGER_H

#include <vector>
#include "GameObject.h"
#include <memory>
#include <iostream>
#include <fstream>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include "Log.h"
#include "Shaders.h"
#include "Physics/PhysicsSystem.h"

class Engine;

class SceneManager {
public:
    SceneManager(std::shared_ptr<PhysicsSystem> physics);
    ~SceneManager();

public:
    bool LoadSceneFromFile(const char* filePath, std::shared_ptr<PhysicsSystem> physics);

    std::vector<Program> mShaderPrograms;

    std::vector<std::shared_ptr<GameObject>> mObjects;
    //std::vector<Light> mLights;   //TODO implement lights

    std::shared_ptr<GameObject> GetRootNode(){ return mSceneRootNode; }

private:
    Program CheckShaderAlreadyLoaded(Shader* v, Shader* f);

    std::shared_ptr<GameObject> mSceneRootNode;

    void ProcessGameObject(const rapidjson::Value& sceneObject, std::shared_ptr<GameObject> parent);

    std::shared_ptr<PhysicsSystem> gPhysicsSystem;
};


#endif //ENGINE_SCENEMANAGER_H
