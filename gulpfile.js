'use strict';

var gulp = require('gulp');
var shell = require('gulp-shell');

gulp.task('compile', shell.task([
    'cmake ./ && make'
]));

gulp.task('copy_assets', shell.task([
    'cp -R assets/ build/assets/'
]));

gulp.task('build', ['compile', 'copy_assets']);