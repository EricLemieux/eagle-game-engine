#include <iostream>

#define GLEW_STATIC
#undef main

#include <Engine.h>
#include <Camera_FPS.h>

int main(int argc, char** argv) {

    //Set the physics system to use the default
    Engine::Instance().SetPhysicsSystem(std::shared_ptr<PhysicsSystem>(new DefaultPhysicsSystem()));

    //Load settings for the window and camera from a config file
    Engine::Instance().GetGraphicsSystem()->LoadSettingsFromConfigFile("assets/config.json");

    //We want to have graphics so we need to create the system for that as well
    Engine::Instance().GetGraphicsSystem()->Create();

    //This is where we load the scene file that describes what game objects exists
    Engine::Instance().GetSceneManager()->LoadSceneFromFile("assets/scenes/test.json", Engine::Instance().GetPhysicsSystem());

    //This generates a debug grid that we can use to get a better sense of scale in our scene
    Engine::Instance().GetGraphicsSystem()->GenerateGrid(1.0f, 10.0f);

    //This something that I was using for debugging and passing variables to a shader
    Engine::Instance().GetSceneManager()->mObjects[0]->SetSelected(true);

    //This is used for debugging by writing the errors and when they happened into the log
    Engine::Instance().GetGraphicsSystem()->CheckForOpenGLErrors("Just before starting the game loop.");

    //You still have direct controll over OpenGL variables
    glEnable(GL_DEPTH_TEST);

    //Going to use the fps camera for this
    std::shared_ptr<Camera_FPS> fpsCam = std::shared_ptr<Camera_FPS>(new Camera_FPS());
    fpsCam->SetInputManager(Engine::Instance().GetInputManager());
    Engine::Instance().SetCamera(fpsCam);


    //This is the main game loop for the project
    while(Engine::Instance().mGameRunning)
    {
        //Update positions and stuff
        Engine::Instance().Update();

        //Render your graphics to the screen 
        Engine::Instance().GetGraphicsSystem()->PreRender();
        Engine::Instance().GetGraphicsSystem()->Render(Engine::Instance().GetSceneManager());
        Engine::Instance().GetGraphicsSystem()->PostRender();
    }

    //Write the debug logs to disk so we can check them
    Engine::Instance().GetLog()->WriteToFile();

    //Close the program
    return 0;
}
