#include <iostream>

#define GLEW_STATIC
#undef main

#include <Engine.h>

int main(int argc, char** argv) {

    //Create the engine object that we are going to use for this project
    auto myEngine = std::make_shared<Engine>();

    //Set the physics system to use the default
    auto physicsSystem = std::make_shared<PhysicsSystem>();
    myEngine->SetPhysicsSystem(physicsSystem);

    //Create 2 phyics boxes and test their collision
    auto boxA = std::make_shared<CollisionBox>();
    boxA->SetPosition(glm::vec3(0,0,0));
    boxA->SetDimentions(glm::vec3(1,1,1));
    auto boxB = std::make_shared<CollisionBox>();
    boxB->SetPosition(glm::vec3(0.9f,0,0));
    boxB->SetDimentions(glm::vec3(1,1,1));

    bool boxColliding = physicsSystem->CollisionTest(boxA, boxB);

    printf("BoxBox colliding = %s\n\n", boxColliding ? "True":"False");

    //Create 2 physics spheres
    auto sphereA = std::make_shared<CollisionSphere>();
    sphereA->SetPosition(glm::vec3(0.0f,0.0f,0.0f));
    sphereA->SetRadius(0.5f);
    auto sphereB = std::make_shared<CollisionSphere>();
    sphereB->SetPosition(glm::vec3(0.9f,0.0f,0.0f));
    sphereB->SetRadius(0.5f);

    bool sphereColliding = physicsSystem->CollisionTest(sphereA, sphereB);

    printf("SphereSphere colliding = %s\n\n", sphereColliding ? "True":"False");

    //Create 1 physics box and 1 physics sphere
    auto boxC = std::make_shared<CollisionBox>();
    boxC->SetPosition(glm::vec3(0,0,0));
    boxC->SetDimentions(glm::vec3(1,1,1));
    auto sphereC = std::make_shared<CollisionSphere>();
    sphereC->SetPosition(glm::vec3(0.9f,0.0f,0.0f));
    sphereC->SetRadius(0.5f);

    bool boxSphereColliding = physicsSystem->CollisionTest(boxC, sphereC);

    printf("SphereSphere colliding = %s\n\n", boxSphereColliding ? "True":"False");

    //Write the debug logs to disk so we can check them
    myEngine->GetLog()->WriteToFile();

    //Close the program
    return 0;
}
