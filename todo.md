# Things to do
This is a todo list that will be a running list of things that are being developed in the project to help keep track of whats going on.

## High priority:
 - Physics

## Medium priority:
- Figure out how to make this more of an entity component system
- Maya style camera movement
- Other 3D file type support/more complete obj support
- Ability to get forward, up, and right vectors for different matrices
- Collision Bodies Renderables for visual debugging
- docs

## Low priority:
- naming schemes, and code style, should be defined and then made the same everywhere
- Figure out a way to make the tab sizing stay the same because right now some of them are 4 and some are 2. Should probably stick with spacing of 2.
