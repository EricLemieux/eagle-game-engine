#version 130

in vec2 texCoords;
in vec3 normals;

uniform float highLight;
uniform sampler2D diffuseTexture;

out vec4 outColour;

void main()
{
    outColour = vec4(texture2D(diffuseTexture, texCoords.xy).rgb, 1.0);
    //outColour = vec4(texCoords.rg, 0.0, 1.0);
}