#version 130

in vec2 texCoords;
in vec3 normals;

uniform float highLight;

out vec4 outColour;

void main()
{
    outColour = vec4(normals.xy, 1.0f, 1.0);
}