#version 130

//I know I am going to forget again so the position is the position of the vertex, its not a weird duplicate of the model translation
in vec3 position;
in vec2 texcoord;
in vec3 normal;

out vec2 texCoords;
out vec3 normals;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    texCoords = texcoord;
    normals = normal;
    gl_Position = proj * view * model * vec4(position,1);
}